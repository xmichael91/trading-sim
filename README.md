# Stock Market Simulator

Coding Challenge Task solution by Michael Kudukin

## Compiling and running

The repository contains a single Eclipse Java (Maven) Project. If you are using Eclipse IDE you can just clone the repository, import `/stock-market-simulator` project, and run it as "Java Application". If the IDE somehow fails to find a main class automatically, this is it: `com.example.stockmarket.simulator.app.Main`

Alternatively you can build a runnable JAR with plain Maven:

`mvn clean verify`

and then run it:

`java -jar target/stock-market-simulator-0.0.1-SNAPSHOT.jar`

## Usage

Application starts, prints some information to the console and then waits for the user input. There are also few hard-coded orders to make testing easier.

You can use commands:
```
add <SYMBOL> <B|S> <QUANTITY> <PRICE>
cancel <ORDER_ID>
quit
```
for adding a new order, cancelling previously added order and quitting application respectively.

For new Symbols, OrderBooks are created automatically. MatchingEngine matches orders in the OrderBooks every second, executes trades and stores them into the TradeLedger.
