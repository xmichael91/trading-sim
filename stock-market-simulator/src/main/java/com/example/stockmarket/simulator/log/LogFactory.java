package com.example.stockmarket.simulator.log;

public class LogFactory
{

	public static boolean USE_NOOP_LOG = false;
	
	public static ILog getLog(Class<?> component)
	{
		if(USE_NOOP_LOG)
			return new NoopLog(component);
		else
			return new SysoutLog(component);
	}

}
