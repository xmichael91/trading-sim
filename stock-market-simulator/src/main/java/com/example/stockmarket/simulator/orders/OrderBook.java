package com.example.stockmarket.simulator.orders;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import com.example.stockmarket.simulator.log.ILog;
import com.example.stockmarket.simulator.log.LogFactory;

/** 
 * Contains all orders for a certain stock
 */
public class OrderBook
{
	private static final ILog LOG = LogFactory.getLog(OrderBook.class);
	
	private final OrderBookSymbol symbol;
	private final Map<OrderId,Order> orders;
	private long lastOrderId = 0;

	/** Constructs new instance
	 * @param symbol the order book symbol to assign
	 */
	public OrderBook(OrderBookSymbol symbol)
	{
		this.symbol = Objects.requireNonNull(symbol);
		orders = new HashMap<>();
	}
	
	/**
	 * @return the order book symbol
	 */
	public OrderBookSymbol getSymbol()
	{
		return symbol;
	}

	/** Create new order and add it to the book
	 * @param type the order type
	 * @param qty the order stock quantity
	 * @param price the price per stock
	 * @return the unique order ID
	 */
	public OrderId addOrder(Order.Type type, int qty, int price)
	{
		synchronized(orders)
		{
			OrderId new_id = new OrderId(symbol, ++lastOrderId);
			Order order = new Order(new_id, type, symbol, qty, price, Instant.now().toEpochMilli());
			orders.put(new_id, order);
			
			LOG.log("Order added: {0}", order);
			return new_id;
		}
	}

	/** Remove the order from the book. Note that to cancel the order
	 * {@link #cancelOrder(OrderId)} should be used instead
	 * @param orderId the order ID
	 * @return true if the order with the orderId was removed from the book,
	 * false otherwise (no order with the orderId present in the book)
	 */
	public boolean removeOrder(OrderId orderId)
	{
		LOG.log("removing order with ID {0}", orderId);
		return doRemoveOrder(orderId) != null;
	}

	private Order doRemoveOrder(OrderId orderId)
	{
		synchronized(orders)
		{
			return orders.remove(orderId);
		}
	}
	
	/** Cancel and remove the order from the book
	 * @param orderId the order ID
	 * @return true if the order with the orderId was removed from the book,
	 * false otherwise (no order with the orderId present in the book)
	 */
	public boolean cancelOrder(OrderId orderId)
	{
		Order order = doRemoveOrder(orderId);
		if(order == null)
			return false;
		
		synchronized (order)
		{
			order.cancel();
		}
		
		LOG.log("Order cancelled: {0}", order);
		return true;
	}
	
	/** Returns the snapshot of the book's order list 
	 * @return the list of orders contained in this book
	 */
	public Collection<Order> getOrders()
	{
		synchronized(orders)
		{
			return new ArrayList<>(orders.values());
		}
	}

}
