package com.example.stockmarket.simulator.orders;

/** 
 * The Order
 */
public class Order
{
	public enum Type
	{
		SELL, BUY
	}
	
	private final OrderId id;
	private final OrderBookSymbol stock;
	private final Type type;
	private final int price;
	private final long timestamp;
	private final int quantity;
	private int filledQuantity;
	private boolean cancelled;

	/** Create new Order
	 * @param id the order ID
	 * @param type the order type {@link Order.Type#SELL} or {@link Order.Type#BUY}
	 * @param stock the stock of the order
	 * @param quantity the quantity of the stock of the order
	 * @param price the price per stock of the order
	 * @param timestamp the UTC timestamp of the order (milliseconds from the epoch)
	 */
	Order(OrderId id, Type type, OrderBookSymbol stock, int quantity, int price, long timestamp)
	{
		this.id = id;
		this.stock = stock;
		this.type = type;
		this.price = price;
		this.quantity = quantity;
		this.timestamp = timestamp;
	}

	/**
	 * @return the order ID
	 */
	public OrderId getId()
	{
		return id;
	}

	/**
	 * @return the stock of the order
	 */
	public OrderBookSymbol getStock()
	{
		return stock;
	}

	/**
	 * @return the type of the order
	 */
	public Type getType()
	{
		return type;
	}

	/**
	 * @return the price per stock of the order
	 */
	public int getPrice()
	{
		return price;
	}

	/**
	 * @return the remaining quantity of the stock of the order
	 * (original asked quantity minus filled quantity)
	 */
	public int getQuantity()
	{
		return quantity - filledQuantity;
	}
	
	/**
	 * @return the original asked quantity of the stock of the order
	 */
	public int getOriginalQuantity()
	{
		return quantity;
	}
	
	/**
	 * @return the already filled quantity of the stock of the order
	 */
	public int getFilledQuantity()
	{
		return filledQuantity;
	}

	/** Increments order's filled quantity by the given value
	 * @param filledQuantity the value of the fill quantity to increment by
	 * @throws IllegalArgumentException if this increment will make total filled
	 * quantity exceed the original asked quantity
	 */
	public void fillQuantity(int filledQuantity)
	{
		if((this.filledQuantity + filledQuantity) > quantity)
			throw new IllegalArgumentException("Too much filled quantity");
		this.filledQuantity += filledQuantity;
	}

	/**
	 * @return whether this order is fully filled
	 */
	public boolean isFulfilled()
	{
		return quantity == filledQuantity;
	}
	
	/**
	 * @return the UTC timestamp of the order (milliseconds from the epoch)
	 */
	public long getTimestamp()
	{
		return timestamp;
	}

	/**
	 * @return whether the order is cancelled
	 */
	public boolean isCancelled()
	{
		return cancelled;
	}

	/**
	 * Cancel the order
	 */
	void cancel()
	{
		cancelled = true;
	}
	
	@Override
	public String toString()
	{
		return "Order ID " + id + " [" + stock.getCode() + " " + type + " " +
				getQuantity() + " @ " + price + "]";
	}
	
}
