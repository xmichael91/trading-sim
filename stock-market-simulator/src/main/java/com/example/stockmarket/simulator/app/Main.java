package com.example.stockmarket.simulator.app;

import com.example.stockmarket.simulator.gateway.ITradingGateway;
import com.example.stockmarket.simulator.gateway.internal.TradingGateway;
import com.example.stockmarket.simulator.matching.internal.MatchingEngine;
import com.example.stockmarket.simulator.orders.IOrderBookManager;
import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.internal.OrderBookManager;
import com.example.stockmarket.simulator.trade.ITradeLedger;
import com.example.stockmarket.simulator.trade.internal.TradeLedger;

/** Stock Market Simulator Application entry point
 * @author Michael Kudukin
 */
public class Main
{
	private static final int BALANCE_PERIOD_SEC = 1;

	public static void main(String[] args)
	{
		System.out.println("Stock Market Simulator ===");

		// initialize components
		IOrderBookManager bookManager = new OrderBookManager();
		ITradingGateway gw = new TradingGateway(bookManager);
		ITradeLedger tradeLedger = new TradeLedger();
		MatchingEngine matcher = new MatchingEngine(bookManager, tradeLedger, BALANCE_PERIOD_SEC);
		
		// add sample orders
		gw.addOrder(Order.Type.BUY, "AAPL", 3, 300);
		gw.addOrder(Order.Type.BUY, "AAPL", 2, 297);
		gw.addOrder(Order.Type.BUY, "AAPL", 5, 300);
		gw.addOrder(Order.Type.BUY, "AAPL", 1, 301);
		
		gw.addOrder(Order.Type.SELL, "AAPL", 2, 304);
		gw.addOrder(Order.Type.SELL, "AAPL", 1, 302);
		gw.addOrder(Order.Type.SELL, "AAPL", 2, 305);
		gw.addOrder(Order.Type.SELL, "AAPL", 4, 304);
		
		// run matcher and command interface
		matcher.run();
		new CLI(gw).run();
		
		// CLI stopped at this point. Stop MatchingEngine and we're done
		matcher.stop();
		System.out.println("Shutting down.");
	}
}
