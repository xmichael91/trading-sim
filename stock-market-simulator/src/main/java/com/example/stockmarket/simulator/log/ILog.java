package com.example.stockmarket.simulator.log;

public interface ILog
{

	void log(String message, Object... details);

}