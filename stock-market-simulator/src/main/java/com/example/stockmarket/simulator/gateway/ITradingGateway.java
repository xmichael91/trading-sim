package com.example.stockmarket.simulator.gateway;

import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.OrderId;
import com.example.stockmarket.simulator.orders.Order.Type;

/** 
 * The Client's Trading Gateway providing methods to place and cancel Orders
 */
public interface ITradingGateway
{
	/** Add Order to the Order Book associated with the symbol, creating the Book
	 * if it is not present
	 * @param type the type of the Order: {@link Order.Type#SELL} or {@link Order.Type#BUY}
	 * @param symbol the Order Book Symbol code (stock code)
	 * @param qty the quantity of shares in Order
	 * @param price the limit price of Order
	 * @return the Order's ID
	 */
	OrderId addOrder(Type type, String symbol, int qty, int price);
	
	/** Cancel the Order
	 * @param orderId the Order's ID
	 * @return true if the Order was cancelled, or false if no Order with this ID
	 * present in the Order Book (i.e. Order was already fulfilled, cancelled earlier or
	 * never been added)
	 */
	boolean cancelOrder(OrderId orderId);
}
