package com.example.stockmarket.simulator.trade.internal;

import com.example.stockmarket.simulator.orders.Order;

/** 
 * The Trade record
 */
public class Trade
{
	private final long id;
	private final Order buyOrder;
	private final Order sellOrder;
	private final int tradePrice;

	/**
	 * @param id the Trade ID
	 * @param buyOrder the BUY order
	 * @param sellOrder the SELL order
	 * @param tradePrice the Trade price
	 */
	public Trade(long id, Order buyOrder, Order sellOrder, int tradePrice)
	{
		this.id = id;
		this.buyOrder = buyOrder;
		this.sellOrder = sellOrder;
		this.tradePrice = tradePrice;
	}

	/**
	 * @return the Trade ID
	 */
	public long getId()
	{
		return id;
	}

	/**
	 * @return the BUY order
	 */
	public Order getBuyOrder()
	{
		return buyOrder;
	}

	/**
	 * @return the SELL order
	 */
	public Order getSellOrder()
	{
		return sellOrder;
	}

	/**
	 * @return the Trade price
	 */
	public int getTradePrice()
	{
		return tradePrice;
	}

	@Override
	public String toString()
	{
		return "Trade [id=" + id + ", buyOrder.id=" + buyOrder.getId() + ", sellOrder.id=" + sellOrder.getId() + ", tradePrice=" + tradePrice
				+ "]";
	}

}
