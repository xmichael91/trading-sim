package com.example.stockmarket.simulator.log;

class NoopLog extends Log
{

	public NoopLog(Class<?> component)
	{
		super(component);
	}

	@Override
	public void log(String message, Object... details)
	{
		// do nothing
	}

}
