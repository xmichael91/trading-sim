package com.example.stockmarket.simulator.orders;

import java.util.Collection;

/** 
 * Contains Order Books associated with their respective Order Book Symbols
 */
public interface IOrderBookManager
{
	/** Finds Order Book by Symbol
	 * @param symbol the Symbol of the Book
	 * @return an Order Book instance or null if no Order Book with such Symbol found
	 */
	OrderBook getBook(OrderBookSymbol symbol);
	
	/** Finds Order Book by Symbol, creating it if not found
	 * @param symbol the Symbol of the Book
	 * @return an existing or new Order Book instance
	 */
	OrderBook getOrCreateBook(OrderBookSymbol symbol);
	
	/**
	 * @return the list of the Order Books contained by this manager
	 */
	Collection<OrderBook> getBooks();
}
