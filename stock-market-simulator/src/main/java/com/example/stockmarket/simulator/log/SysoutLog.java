package com.example.stockmarket.simulator.log;

import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

class SysoutLog extends Log
{
	private SimpleDateFormat timestampFormat;

	public SysoutLog(Class<?> component)
	{
		super(component);
		timestampFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	}
	
	@Override
	public void log(String message, Object...details)
	{
		synchronized(System.out)
		{
			System.out.print("[");
			System.out.print(timestampFormat.format(new Date()));
			System.out.print("]");
			System.out.print("[");
			System.out.print(componentId);
			System.out.print("] ");
			System.out.println(MessageFormat.format(message, details));
		}
	}
}
