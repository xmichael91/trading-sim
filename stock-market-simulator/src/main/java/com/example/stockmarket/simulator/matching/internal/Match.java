package com.example.stockmarket.simulator.matching.internal;

import com.example.stockmarket.simulator.orders.Order;

/** 
 * The match of two BUY-SELL orders
 */
class Match
{
	public final Order buyOrder;
	public final Order sellOrder;

	public Match(Order buyOrder, Order sellOrder)
	{
		this.buyOrder = buyOrder;
		this.sellOrder = sellOrder;
	}
}