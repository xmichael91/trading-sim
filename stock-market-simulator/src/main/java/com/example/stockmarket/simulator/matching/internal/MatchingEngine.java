package com.example.stockmarket.simulator.matching.internal;

import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.example.stockmarket.simulator.log.ILog;
import com.example.stockmarket.simulator.log.LogFactory;
import com.example.stockmarket.simulator.orders.IOrderBookManager;
import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.OrderBook;
import com.example.stockmarket.simulator.trade.ITradeLedger;

/** The Exchange Matching Engine. Balances the Order Books with certain time
 * interval by matching Orders and executing Trades 
 */
public class MatchingEngine
{
	private static final ILog LOG = LogFactory.getLog(MatchingEngine.class);

	private static final int SHUTDOWN_TIMEOUT_SEC = 15;
	
	private final IOrderBookManager bookManager;
	private final ITradeLedger tradeLedger;
	private final int balancePeriodSec;
	private final OrderMatcher orderMatcher;
	private ScheduledExecutorService executor;

	/** Create new instance
	 * @param bookManager the Order Book Manager which books will be balanced
	 * @param tradeLedger the Trade Ledger that will be used for storing executed Trades
	 * @param balancePeriodSec Order Book balancing period in seconds, must be equal or greater than 1
	 */
	public MatchingEngine(IOrderBookManager bookManager, ITradeLedger tradeLedger, int balancePeriodSec)
	{
		this.bookManager = Objects.requireNonNull(bookManager);
		this.tradeLedger = Objects.requireNonNull(tradeLedger);
		if(balancePeriodSec < 1)
			throw new IllegalArgumentException("Invalid balance period: " + balancePeriodSec);
		
		this.balancePeriodSec = balancePeriodSec;
		orderMatcher = new OrderMatcher();
	}

	/**
	 * Run the Matching Engine. The Engine runs in its own separate thread hence this metod
	 * does not block
	 */
	public synchronized void run()
	{
		// check if already running
		if(executor != null && !executor.isShutdown())
		{
			LOG.log("Subsequent run() ignored");
			return;
		}
		
		// configure and run scheduled executor
		executor = Executors.newSingleThreadScheduledExecutor(task -> new Thread(task, "MatchingEngine"));
		executor.scheduleAtFixedRate(this::balanceBooks, 0, balancePeriodSec, TimeUnit.SECONDS);
		
		LOG.log("Started");
	}
	
	/**
	 * Attempts to stop the Matching Engine and awaits for its termination for some timeout
	 */
	public synchronized void stop()
	{
		// check if already stopped or not started at all
		if(executor != null && !executor.isShutdown())
		{
			LOG.log("Stopping...");
			executor.shutdown();
			try
			{
				if(!executor.awaitTermination(SHUTDOWN_TIMEOUT_SEC, TimeUnit.SECONDS))
					LOG.log("Executor termination timed out");
				else
					LOG.log("Stopped");
			}
			catch(InterruptedException e)
			{
				// ok, ignore
			}
		}
	}
	
	private void balanceBooks()
	{
		for(OrderBook book : bookManager.getBooks())
		{
			try
			{
				balanceBook(book);
			}
			catch(Exception e)
			{
				LOG.log("An error happened while balancing book {0}: ({1}) {2}", 
						book, e.getClass().getSimpleName(), e.getMessage());
			}
		}
	}

	private void balanceBook(OrderBook book)
	{
		Collection<Order> orders = book.getOrders();
		
		while(true) // repeat until there is no match
		{
			Match match = orderMatcher.matchOrders(orders);
			if(match == null)
				return;
			
			// lock on orders to prevent their mid-trade cancellation
			synchronized (match.buyOrder)
			{
				synchronized (match.sellOrder)
				{
					// check orders is not cancelled
					if(match.buyOrder.isCancelled() || match.sellOrder.isCancelled())
					{
						// remove cancelled orders from the current order list snapshot
						if(match.buyOrder.isCancelled())
							orders.remove(match.buyOrder);
						if(match.sellOrder.isCancelled())
							orders.remove(match.sellOrder);
						
						// and try matching again
						continue;
					}
					
					makeTrade(match.buyOrder, match.sellOrder);
				}
			}
			
			// after successful trade, remove orders that have been fully filled
			// from the Book and from the current order list snapshot
			removeOrderIfFullfilled(book, orders, match.buyOrder);
			removeOrderIfFullfilled(book, orders, match.sellOrder);
		}
	}

	private void removeOrderIfFullfilled(OrderBook book, Collection<Order> orders, Order order)
	{
		if(order.isFulfilled())
		{
			LOG.log("Fully filled order with ID {0}", order.getId());
			orders.remove(order);
			book.removeOrder(order.getId());
		}
		else
		if(order.getFilledQuantity() > 0)
		{
			LOG.log("Partially filled order with ID {0}: {1} of {2} shares remain", order.getId(),
					order.getQuantity(), order.getOriginalQuantity());
		}
	}

	private void makeTrade(Order buyOrder, Order sellOrder)
	{
		// the amount of the shares that will be traded is the minimum quantity of two orders
		int tradeQuantity = Math.min(buyOrder.getQuantity(), sellOrder.getQuantity());
		
		// fill orders with this amount
		buyOrder.fillQuantity(tradeQuantity);
		sellOrder.fillQuantity(tradeQuantity);
		
		// the older order's price will be used for the trade,
		// the newer one will get the best trade price
		int tradePrice = buyOrder.getTimestamp() < sellOrder.getTimestamp() ?
				buyOrder.getPrice() :
				sellOrder.getPrice();

		tradeLedger.storeTrade(buyOrder, sellOrder, tradeQuantity, tradePrice);
	}
	
}
