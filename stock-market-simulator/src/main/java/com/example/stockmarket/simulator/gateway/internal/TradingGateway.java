package com.example.stockmarket.simulator.gateway.internal;

import java.util.Objects;

import com.example.stockmarket.simulator.gateway.ITradingGateway;
import com.example.stockmarket.simulator.orders.IOrderBookManager;
import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.OrderBook;
import com.example.stockmarket.simulator.orders.OrderBookSymbol;
import com.example.stockmarket.simulator.orders.OrderId;

/** 
 * The default Trading Gateway implementation
 */
public class TradingGateway implements ITradingGateway
{
	private final IOrderBookManager bookManager;

	/**
	 * @param bookManager the Order Book Manager to work with
	 */
	public TradingGateway(IOrderBookManager bookManager)
	{
		this.bookManager = Objects.requireNonNull(bookManager);
	}
	
	@Override
	public OrderId addOrder(Order.Type type, String symbol, int qty, int price)
	{
		Objects.requireNonNull(type);
		if(symbol.isEmpty())
			throw new IllegalArgumentException("Empty Symbol is not allowed");
		if(qty <= 0)
			throw new IllegalArgumentException("Invalid quantity: " + qty);
		if(price <= 0)
			throw new IllegalArgumentException("Invalid price: " + price);

		OrderBookSymbol bookSymbol = new OrderBookSymbol(symbol);
		OrderBook book = bookManager.getOrCreateBook(bookSymbol);
		
		return book.addOrder(type, qty, price);
	}

	@Override
	public boolean cancelOrder(OrderId orderId)
	{
		OrderBook book = bookManager.getBook(orderId.getSymbol());
		if(book != null)
			return book.cancelOrder(orderId);
		
		return false;
	}

}
