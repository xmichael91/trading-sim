package com.example.stockmarket.simulator.matching.internal;

import java.util.Collection;
import java.util.Comparator;

import com.example.stockmarket.simulator.orders.Order;

/** 
 * The default implementation of the order matcher.
 * Matches two orders from the list following a price/time priority
 * and a limit price rules
 */
class OrderMatcher
{
	/**
	 * Comparator taking higher price or older timestamp as higher priority
	 */
	public static final Comparator<? super Order> BEST_BUY_COMPARATOR = (a, b) ->
	{
		int price_diff = a.getPrice() - b.getPrice();
		if(price_diff != 0)
			return price_diff;
		else
			return a.getTimestamp() < b.getTimestamp() ? 1 : -1;
	};

	/**
	 * Comparator taking lower price or older timestamp as higher priority
	 */
	public static final Comparator<? super Order> BEST_SELL_COMPARATOR = (a, b) ->
	{
		int price_diff = b.getPrice() - a.getPrice();
		if(price_diff != 0)
			return price_diff;
		else
			return a.getTimestamp() < b.getTimestamp() ? 1 : -1;
	};
	
	/** Match two orders for a trade
	 * @param orders the list of orders, two of which may be matched for a trade
	 * @return the match containing two orders or null if there is no match in the list
	 */
	public Match matchOrders(Collection<Order> orders)
	{
		// find a BUY order with a highest price/time priority
		Order bestBuy = orders.stream()
			.filter(o -> o.getType().equals(Order.Type.BUY))
			.max(BEST_BUY_COMPARATOR).orElse(null);
		
		if(bestBuy == null)
			return null;
		
		// find SELL orders matching price with the BUY order found earlier,
		// and select the one that have highest price/time priority
		Order bestMatchedSell = orders.stream()
			.filter(o -> o.getType().equals(Order.Type.SELL))
			.filter(o -> o.getPrice() <= bestBuy.getPrice())
			.max(BEST_SELL_COMPARATOR).orElse(null);
		
		if(bestMatchedSell == null)
			return null;
		
		return new Match(bestBuy, bestMatchedSell);
	}
}
