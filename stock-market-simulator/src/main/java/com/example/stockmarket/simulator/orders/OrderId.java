package com.example.stockmarket.simulator.orders;

import java.util.Objects;

/** 
 * The order ID based on Order Book Symbol and order's ordinal number
 */
public class OrderId
{
	private final OrderBookSymbol bookSymbol;
	private final long ordinal;

	/**
	 * @param bookSymbol the symbol of the order book containing the order
	 * @param ordinal the ordinal number of the order within its book
	 */
	public OrderId(OrderBookSymbol bookSymbol, long ordinal)
	{
		this.bookSymbol = Objects.requireNonNull(bookSymbol);
		this.ordinal = ordinal;
	}
	
	/**
	 * @return the symbol of the order book containing the order
	 */
	public OrderBookSymbol getSymbol()
	{
		return bookSymbol;
	}
	
	@Override
	public int hashCode()
	{
		return Objects.hash(bookSymbol, ordinal);
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		OrderId other = (OrderId) obj;
		return Objects.equals(bookSymbol, other.bookSymbol) && ordinal == other.ordinal;
	}
	
	@Override
	public String toString()
	{
		return bookSymbol + "#" + ordinal;
	}
}
