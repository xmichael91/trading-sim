package com.example.stockmarket.simulator.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import com.example.stockmarket.simulator.gateway.ITradingGateway;
import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.OrderBookSymbol;
import com.example.stockmarket.simulator.orders.OrderId;

/** 
 * Command Line Interface utility. Handles user input
 */
public class CLI
{

	private final ITradingGateway tradingGateway;
	private final Map<String,Consumer<String>> commandHandlers;
	private boolean quit;

	/**
	 * @param tradingGateway the Trading Gateway to work with
	 */
	public CLI(ITradingGateway tradingGateway)
	{
		this.tradingGateway = tradingGateway;
		
		commandHandlers = new HashMap<>();
		commandHandlers.put("add", this::handleAdd);
		commandHandlers.put("cancel", this::handleCancel);
		commandHandlers.put("quit", this::handleQuit);
	}

	/**
	 * Run interactive command handler. This metod blocks until the user
	 * issue "quit" command or some IO error occur
	 */
	public void run()
	{
		try(BufferedReader r = new BufferedReader(new InputStreamReader(System.in)))
		{
			System.out.println("Command Line Interface started.");
			System.out.println("Available commands are: " +
					commandHandlers.keySet().stream().collect(Collectors.joining(", ")));
			System.out.println("Type command name to find out usage pattern (except \"quit\")");
			System.out.println();
			
			while(!quit)
			{
				System.out.print("> ");
				String line = r.readLine();
				if(line != null)
				{
					// separate command from its arguments
					String[] cmd = line.split(" ", 2);
					Consumer<String> handler = commandHandlers.get(cmd[0]);
					if(handler == null)
					{
						System.out.println("Unknown command");
						continue;
					}
					
					String args = cmd.length > 1 ? cmd[1] : "";
					try
					{
						handler.accept(args);
					}
					catch(Exception e)
					{
						System.out.println("An error occurred: " + e.getClass().getSimpleName() + " : " + e.getMessage());
					}
				}
			}
		} catch (IOException e1)
		{
			e1.printStackTrace();
		}
	}

	private void handleAdd(String argsStr)
	{
		if(argsStr.isEmpty())
		{
			System.out.println("Adds order: add <SYMBOL> <B|S> <QUANTITY> <PRICE>");
			return;
		}
		final int ARG_COUNT = 4;
		String[] args = argsStr.split(" ", ARG_COUNT + 1);
		if(args.length != ARG_COUNT)
			throw new IllegalArgumentException("Invalid argument count (example: \"add GOOG B 100 50\")");
		
		Order.Type t;
		if("S".equals(args[1]))
			t = Order.Type.SELL;
		else
		if("B".equals(args[1]))
			t = Order.Type.BUY;
		else
			throw new IllegalArgumentException("Invalid order type. Must be S or B");
		
		tradingGateway.addOrder(t, args[0], Integer.parseInt(args[2]), Integer.parseInt(args[3]));
	}

	private void handleCancel(String args)
	{
		if(args.isEmpty())
		{
			System.out.println("Cancels order: cancel <ORDER_ID>");
			return;
		}
		String[] parts = args.split("#", 2);
		if(parts.length != 2)
			throw new IllegalArgumentException("Invalid Order ID format (example: \"cancel GOOG#6\")");
		OrderId id = new OrderId(new OrderBookSymbol(parts[0]), Long.parseLong(parts[1]));
		tradingGateway.cancelOrder(id);
	}

	private void handleQuit(String args)
	{
		quit = true;
	}

}
