package com.example.stockmarket.simulator.trade.internal;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.example.stockmarket.simulator.log.ILog;
import com.example.stockmarket.simulator.log.LogFactory;
import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.trade.ITradeLedger;

/** 
 * The default Trade Ledger implementation
 */
public class TradeLedger implements ITradeLedger
{
	private static final ILog LOG = LogFactory.getLog(TradeLedger.class);
	
	private List<Trade> trades = new ArrayList<>();
	private long lastTradeId;
	
	@Override
	public void storeTrade(Order buyOrder, Order sellOrder, int tradeQuantity, int tradePrice)
	{
		Objects.requireNonNull(buyOrder);
		Objects.requireNonNull(sellOrder);
		if(tradeQuantity <= 0)
			throw new IllegalArgumentException("Invalid quantity: " + tradeQuantity);
		if(tradePrice <= 0)
			throw new IllegalArgumentException("Invalid price: " + tradePrice);
		
		long newId;
		synchronized (trades)
		{
			newId = ++lastTradeId;
			Trade trade = new Trade(newId, buyOrder, sellOrder, tradePrice);
			trades.add(trade);
		}
		
		LOG.log("New Trade ID {0}: {1} {2} @ {3} (orders {4} and {5})", newId, buyOrder.getStock().getCode(),
				tradeQuantity, tradePrice, buyOrder.getId(), sellOrder.getId());
	}

	@Override
	public List<Trade> getTrades()
	{
		synchronized (trades)
		{
			return new ArrayList<>(trades);
		}
	}
	
}
