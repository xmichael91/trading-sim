package com.example.stockmarket.simulator.trade;

import java.util.List;

import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.trade.internal.Trade;

/** 
 * Contains executed Trades
 */
public interface ITradeLedger
{
	/** Create and store the Trade record in the Ledger. Note that no Orders will be
	 * be modified by this method.
	 * @param buyOrder buy order
	 * @param sellOrder sell order
	 * @param tradeQuantity traded share quantity
	 * @param tradePrice trade price
	 */
	void storeTrade(Order buyOrder, Order sellOrder, int tradeQuantity, int tradePrice);

	/**
	 * @return the list of the Trades contained by this Ledger
	 */
	List<Trade> getTrades();
}
