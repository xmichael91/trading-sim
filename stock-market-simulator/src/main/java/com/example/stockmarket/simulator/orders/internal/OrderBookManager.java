package com.example.stockmarket.simulator.orders.internal;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

import com.example.stockmarket.simulator.log.ILog;
import com.example.stockmarket.simulator.log.LogFactory;
import com.example.stockmarket.simulator.orders.IOrderBookManager;
import com.example.stockmarket.simulator.orders.OrderBook;
import com.example.stockmarket.simulator.orders.OrderBookSymbol;

/** 
 * The default Order Book Manager implementation
 */
public class OrderBookManager implements IOrderBookManager
{
	private static final ILog LOG = LogFactory.getLog(OrderBookManager.class);
	
	private Map<OrderBookSymbol,OrderBook> books = new ConcurrentHashMap<>();
	
	@Override
	public OrderBook getBook(OrderBookSymbol symbol)
	{
		return books.get(Objects.requireNonNull(symbol));
	}

	@Override
	public OrderBook getOrCreateBook(OrderBookSymbol symbol)
	{
		return books.computeIfAbsent(Objects.requireNonNull(symbol), s ->
		{
			LOG.log("Creating new Order Book with symbol: {0}", symbol);
			return new OrderBook(s);
		});
	}

	@Override
	public Collection<OrderBook> getBooks()
	{
		return books.values();
	}

}
