package com.example.stockmarket.simulator.log;

abstract class Log implements ILog
{
	protected final String componentId;

	public Log(Class<?> component)
	{
		this.componentId = component.getSimpleName();
	}

}
