package com.example.stockmarket.simulator.orders;

import com.example.stockmarket.simulator.orders.Order;

/** 
 * Test Order class with fixed Stock and quantity, id of type long
 * and generated timestamp based on the id
 */
public class FakeOrder extends Order
{
	private static final OrderBookSymbol AAPL = new OrderBookSymbol("AAPL");
	
	public FakeOrder(long id, Type type, int price)
	{
		super(new OrderId(AAPL, id), type, AAPL, 10, price, id * 1000);
	}
}