package com.example.stockmarket.simulator.matching.internal;

import static com.example.stockmarket.simulator.orders.Order.Type.*;
import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.example.stockmarket.simulator.orders.Order;
import com.example.stockmarket.simulator.orders.FakeOrder;

class OrderMatcherTest
{
	private OrderMatcher matcher;

	@BeforeEach
	void setUpMatcher()
	{
		matcher = new OrderMatcher();
	}
	
	@Test
	void testNullLeadsToNPE()
	{
		assertThrows(NullPointerException.class, () -> matcher.matchOrders(null));
	}
	
	@Test
	void testEmptyReturnsNull()
	{
		assertNull(matcher.matchOrders(Collections.emptyList()));
	}
	
	@Test
	void testOneOrderReturnsNull() throws Exception
	{
		assertNull(matcher.matchOrders(Arrays.asList(new FakeOrder(1, SELL, 300))));
	}
	
	@Test
	void testMatch_EqualPrice_TwoOrdersOnly()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	
				new FakeOrder( 1, BUY,  300)
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(1), match.buyOrder);
	}
	
	@Test
	void testMatch_EqualPrice_WithTwoOrdersUnmatching()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	// <--
				new FakeOrder( 1, BUY,  290),
				new FakeOrder( 2, SELL, 320),
				new FakeOrder( 3, BUY,  300)	// <--
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(3), match.buyOrder);
	}
	
	@Test
	void testMatch_SellFirst() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	
				new FakeOrder( 1, BUY,  320)
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(1), match.buyOrder);
	}
	
	@Test
	void testMatch_BuyFirst()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, BUY,  320),
				new FakeOrder( 1, SELL, 300)	
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.buyOrder);
		assertSame(orders.get(1), match.sellOrder);
	}

	@Test
	void testNoMatch_EqualPrice_OnlySell()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),
				new FakeOrder( 1, SELL, 300)	
			);
		
		assertNull(matcher.matchOrders(orders));
	}
	
	@Test
	void testNoMatch_OnlySell()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 320),
				new FakeOrder( 1, SELL, 300)	
			);
		
		assertNull(matcher.matchOrders(orders));
	}
	
	@Test
	void testNoMatch_SellFirst() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 320),	
				new FakeOrder( 1, BUY,  300)
			);
		
		assertNull(matcher.matchOrders(orders));
	}
	
	@Test
	void testNoMatch_BuyFirst()
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, BUY,  300),
				new FakeOrder( 1, SELL, 320)	
			);
		
		assertNull(matcher.matchOrders(orders));
	}
	
	@Test
	void testMatch_BestBuyPrice() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	// <--
				new FakeOrder( 1, BUY,  320),	// <--
				new FakeOrder( 2, BUY,  300),
				new FakeOrder( 3, BUY,  290)
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(1), match.buyOrder);
	}
	
	@Test
	void testMatch_BestSellPrice() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	
				new FakeOrder( 1, BUY,  320),	// <--
				new FakeOrder( 2, SELL, 320),
				new FakeOrder( 3, SELL, 290)	// <--
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(1), match.buyOrder);
		assertSame(orders.get(3), match.sellOrder);
	}
	
	@Test
	void testMatch_EqualBestBuyPrice_OlderWins() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 300),	// <--
				new FakeOrder( 1, BUY,  320),	// <--
				new FakeOrder( 2, BUY,  300),
				new FakeOrder( 3, BUY,  320)
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(1), match.buyOrder);
	}
	
	@Test
	void testMatch_EqualBestSellPrice_OlderWins() throws Exception
	{
		List<Order> orders = Arrays.asList(
				new FakeOrder( 0, SELL, 290),	// <--
				new FakeOrder( 1, BUY,  320),	// <--
				new FakeOrder( 2, SELL, 320),
				new FakeOrder( 3, SELL, 290)
			);
		
		Match match = matcher.matchOrders(orders);
		assertSame(orders.get(0), match.sellOrder);
		assertSame(orders.get(1), match.buyOrder);
	}
}
